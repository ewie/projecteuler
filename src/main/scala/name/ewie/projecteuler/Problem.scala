package name.ewie.projecteuler

abstract class Problem(val number: Int, val expectedResult: Option[Long]) extends (() => Long) {

  require(number > 0, s"illegal problem number: $number")

  def this(number: Int) = this(number, None)

  def this(number: Int, expectedResult: Long) = this(number, Some(expectedResult))

}
