package name.ewie.projecteuler

object ProjectEuler {

  def main(args: Array[String]) {
    1.to(500).flatMap(problem).foreach(run)
  }

  private def run(problem: Problem) {
    val t0 = System.nanoTime
    val r = execute(problem)
    val td = (System.nanoTime - t0) / 1e9
    val res = result(r)
    val m = mark(r, problem.expectedResult)
    val e = emotion(r, problem.expectedResult)
    println(f"${url(problem)} [$td%8.5f] -> $res%12s $m $e")
  }

  private def result(result: Option[Long]) = result match {
    case None => ""
    case Some(v) => v
  }

  private def execute(problem: Problem) = try {
    Some(problem())
  } catch {
    case e: NoSolutionFound => None
  }

  private def url(problem: Problem) = f"https://projecteuler.net/problem=${problem.number}%03d"

  private def mark(result: Option[Long], expectedResult: Option[Long]) = {
    expectedResult match {
      case None => "?"
      case Some(v) =>
        result match {
          case None => s"\u2718 (expected $v)"
          case Some(w) =>
            if (w == v) "\u2714"
            else s"\u2718 (expected $v)"
        }
    }
  }

  private def emotion(result: Option[Long], expectedResult: Option[Long]) = {
    expectedResult match {
      case None => "?"
      case Some(v) =>
        result match {
          case None => "(ノಠ益ಠ)ノ彡┻━┻"
          case Some(w) =>
            if (w == v) "\\o/"
            else "ಠ_ಠ"
        }
    }
  }

  private def problem(number: Int) = {
    val loader = getClass.getClassLoader
    try {
      val cls = loader.loadClass(f"name.ewie.projecteuler.problems.Problem$number%03d")
      Some(cls.newInstance().asInstanceOf[Problem])
    } catch {
      case e: ClassNotFoundException => None
    }
  }

}
