package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem006 extends Problem(6, 25164150) {

  override def apply() = 1.to(100).sum * 1.to(100).sum - 1.to(100).map{ n => n * n }.sum

}
