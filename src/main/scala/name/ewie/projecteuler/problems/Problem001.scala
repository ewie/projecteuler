package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem001 extends Problem(1, 233168) {

  override def apply() = 0.to(999).filter{ n => n % 3 == 0 || n % 5 == 0 }.sum

}
