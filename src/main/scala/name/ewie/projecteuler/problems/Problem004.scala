package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem004 extends Problem(4, 906609) {

  override def apply() = {
    100.to(999)
      .flatMap{ n => 100.to(999).map(_ * n) }
      .filter{ n => n == reverse(n) }
      .max
  }

  private def reverse(n: Int) = {
    var k = n
    var r = 0
    while (k > 0) {
      r = r * 10 + (k % 10)
      k /= 10
    }
    r
  }

}
