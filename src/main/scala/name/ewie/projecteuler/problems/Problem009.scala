package name.ewie.projecteuler.problems

import name.ewie.projecteuler.{NoSolutionFound, Problem}

class Problem009 extends Problem(9, 31875000) {

  val LIMIT = 1000

  override def apply(): Long = {
    for (a <- 0 to LIMIT;
         b <- (a + 1) to LIMIT;
         c <- (b + 1) to LIMIT) {
      if (a + b + c == LIMIT && a * a + b * b == c * c) {
        return a * b * c
      }
    }
    throw new NoSolutionFound
  }

}
