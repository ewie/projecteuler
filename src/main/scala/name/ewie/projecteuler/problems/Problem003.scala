package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem003 extends Problem(3, 6857) {

  override def apply() = factors(600851475143L).max

  private def factors(n: Long): List[Long] = n match {
    case 1 => Nil
    case _ =>
      val p = ld(n)
      p :: factors(n / p)
  }

  private def ld(n: Long) = ldf(2, n)

  private def ldf(k: Long, n: Long): Long = {
    if (n % k == 0) k
    else if (k * k > n) n
    else ldf(k + 1, n)
  }

}
