package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

import scala.math.sqrt

class Problem010 extends Problem(10, 142913828922L) {

  override def apply() = 2L.to(2000000L).filter(isPrime).sum

  private def isPrime(n: Long) = 2.to(sqrt(n).toInt).forall(n % _ != 0)

}
