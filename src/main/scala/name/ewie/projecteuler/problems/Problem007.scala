package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

import scala.math.sqrt

class Problem007 extends Problem(7, 104743) {

  override def apply() = {
    Stream.from(2)
      .filter(isPrime)
      .zipWithIndex
      .drop(10000)
      .head
      ._1
  }

  private def isPrime(n: Int) = 2.to(sqrt(n).toInt).forall(n % _ != 0)

}
