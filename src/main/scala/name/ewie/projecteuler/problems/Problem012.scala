package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

import scala.math.sqrt

class Problem012 extends Problem(12, 76576500) {

  override def apply() = triangles.filter(divisors(_) > 500).head

  private def triangles = Stream.from(0).map(1.to(_).sum)

  private def divisors(n: Int) = {
    1.to(sqrt(n).toInt).fold(0) { (d, k) =>
      if (n % k == 0) {
        if (k * k == n) d + 1
        else d + 2
      } else d
    }
  }

}
