package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem015 extends Problem(15, 137846528820L) {

  override def apply() = {
    val grid = new Array[Long](21 * 21)
    0.until(21).foreach { i =>
      grid(i) = 1
      grid(i * 21) = 1
    }
    22.until(grid.length).foreach { i =>
      if ((i + 1) % 21 != 1) grid(i) = grid(i - 1) + grid(i - 21)
    }
    grid.last
  }

}
