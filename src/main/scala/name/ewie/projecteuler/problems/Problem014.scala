package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem014 extends Problem(14, 837799) {

  override def apply() = {
    1.to(1000000)
      .map{ n => (n, collatz(n)) }
      .sortBy(_._2)
      .map(_._1)
      .last
  }

  private def collatz(n: Long) = {
    var c = 1
    var k = n
    while (k > 1) {
      c += 1
      k = if (k % 2 == 0) k / 2 else k * 3 + 1
    }
    c
  }

}
