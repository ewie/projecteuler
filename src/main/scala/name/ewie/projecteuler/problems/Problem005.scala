package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem005 extends Problem(5, 232792560) {

  override def apply() = {
    Stream.from(20, 20)
      // No need to check 1 to 10 because 11 to 20 contain multiples of those.
      .filter{ n => 11.to(20).forall(n % _ == 0) }
      .head
  }

}
