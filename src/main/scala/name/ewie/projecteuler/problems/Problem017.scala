package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem017 extends Problem(17, 21124) {

  override def apply() = {
    Problem017.ONE_THOUSAND + 1.to(999)
      .map { n =>
        val _1 = n % 10
        val _10 = n % 100 / 10
        val _100 = n % 1000 / 100
        if (_100 == 0) tens(_10, _1)
        else if (_10 == 0 && _1 == 0) hundred(_100)
        else hundred(_100) + Problem017.AND + tens(_10, _1)
      }
      .sum
  }

  private def hundred(_100: Int) = Problem017.ONES(_100) + Problem017.HUNDRED

  private def tens(_10: Int, _1: Int) = _10 match {
    case 0 => Problem017.ONES(_1)
    case 1 => Problem017.TEENS(_1)
    case _ => Problem017.TENS(_10) + Problem017.ONES(_1)
  }

}

object Problem017 {

  val ONES = 0 :: list("one two three four five six seven eight nine")

  val TEENS = list("ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen")

  val TENS = 0 :: 0 :: list("twenty thirty forty fifty sixty seventy eighty ninety")

  val ONE_THOUSAND = list("one thousand").sum

  val HUNDRED = "hundred".length

  val AND = "and".length

  private def list(s: String) = s.split(" ").map(_.length).toList

}
