package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem016 extends Problem(16, 1366) {

  override def apply() = BigInt(2).pow(1000).toString.map(_ - '0').sum

}
