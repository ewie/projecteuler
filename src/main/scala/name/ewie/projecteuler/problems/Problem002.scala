package name.ewie.projecteuler.problems

import name.ewie.projecteuler.Problem

class Problem002 extends Problem(2, 4613732) {

  override def apply() = fib.takeWhile(_ < 4000000).filter(_ % 2 == 0).sum

  private def fib = fib0(0, 1)

  private def fib0(a: Long, b: Long): Stream[Long] = a #:: fib0(b, a + b)

}
